package com.example.demowithtests.domain;

import com.example.demowithtests.util.annotations.entity.Name;
import com.example.demowithtests.util.annotations.entity.ToLowerCase;
import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "users")
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class Employee {

    @Id
    private Integer id;

    @Name
    private String name;

    private String country;

    @ToLowerCase
    private String email;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "employee_id")
    @OrderBy("id desc, country asc")
    private Set<Address> addresses = new HashSet<>();

    @Enumerated(EnumType.STRING)
    private Gender gender;

    @OneToOne  (cascade = CascadeType.ALL)
    @JoinColumn(name = "document_id", referencedColumnName = "id")
    private Document document;

    @JoinColumn(name = "is_deleted")
    private Boolean isDeleted = false;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "document_history_id", referencedColumnName = "id")
    private DocumentHistory documentHistory;

    @PrePersist
    public void generateId() {
        this.id = Math.abs(name.hashCode() + LocalDateTime.now().hashCode());
    }

    @PreUpdate
    @PreRemove
    public void history() {
        Document oldDocument = this.document;
        Document newDocument = null;
        if (!this.isDeleted) {
            newDocument = this.document;
        }
        DocumentHistory history = new DocumentHistory();
        history.setId(Math.abs(this.id + LocalDateTime.now().hashCode()));
        history.setEmployeeId(this.id);
        history.setDocumentId(this.document.getId());
        history.setEventType(this.isDeleted ? EventType.DELETED : EventType.UPDATED);
        history.setOldDocument(oldDocument);
        history.setNewDocument(newDocument);
        this.documentHistory = history;
    }
}

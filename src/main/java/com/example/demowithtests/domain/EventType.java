package com.example.demowithtests.domain;

public enum EventType {
    DELETED,
    UPDATED
}

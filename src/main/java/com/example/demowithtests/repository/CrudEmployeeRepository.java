package com.example.demowithtests.repository;

import com.example.demowithtests.domain.Employee;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface CrudEmployeeRepository extends JpaRepository<Employee, Integer> {
    @Query("update Employee set name = ?1 where id = ?2")
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    @Transactional
    void updateEmployeeByName(String name, Integer id);

    @Transactional
    @Modifying(flushAutomatically = true, clearAutomatically = true)
    @Query(value = "INSERT INTO users(name, email, country, gender) VALUES (:name, :email, :country, :gender)",
            nativeQuery = true)
    void saveEmployee(String name, String email, String country, String gender);

    @Transactional
    @Modifying(flushAutomatically = true, clearAutomatically = true)
    @Query(value = "UPDATE users SET name = ?1, email = ?2, country = ?3 WHERE id = ?4 and is_deleted != true",
            nativeQuery = true)
    Integer updateEmployee(String name, String email, String country, Integer id);

    @Override
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    @Transactional
    @EntityGraph(type = EntityGraph.EntityGraphType.FETCH, value = "user_entity-graph")
    <S extends Employee> S save(S entity);

    @Override
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    @Transactional
    @EntityGraph(type = EntityGraph.EntityGraphType.FETCH, value = "user_entity-graph")
    <S extends Employee> List<S> saveAll(Iterable<S> entities);

}

package com.example.demowithtests.repository;

import com.example.demowithtests.domain.DocumentHistory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface DocumentHistoryRepository extends JpaRepository<DocumentHistory, Integer> {
    Optional<DocumentHistory> findById(Integer id);

    List<DocumentHistory> findAll();

    List<DocumentHistory> findAllByEmployeeId(Integer employeeId);

    List<DocumentHistory> findAllByDocumentId(Integer documentId);

    List<DocumentHistory> findAllByEventType(String eventType);

    DocumentHistory save(DocumentHistory documentHistory);

    void delete(DocumentHistory documentHistory);
}

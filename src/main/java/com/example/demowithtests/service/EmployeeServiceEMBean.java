package com.example.demowithtests.service;

import com.example.demowithtests.domain.Employee;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Query;
import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Service
public class EmployeeServiceEMBean implements EmployeeServiceEM {

    @PersistenceContext
    private EntityManager entityManager;

    /**
     * @param employee
     * @return
     */
    @Override
    @Transactional //jakarta
    public Employee createWithJpa(Employee employee) {
        employee.setIsDeleted(false);
        return entityManager.merge(employee);
    }

    @Override
    @Transactional //persist
    public Employee createWithJpaPersist(Employee employee) {
        employee.setIsDeleted(false);
        entityManager.persist(employee);
        return employee;
    }

    @Override
    @Transactional //flush
    public Employee createWithJpaFlush(Employee employee) {
        employee.setIsDeleted(false);
        entityManager.flush();
        return employee;
    }

    @Override
    @Transactional // query
    public Employee createWithJpaQuery(Employee employee) {
        try {
            String nativeQuery =
                    "INSERT INTO Employee (id, name, country, email, addresses, gender, isDeleted) " +
                            "VALUES (?, ?, ?, ?, ?, ?, ?)";
            Query query = entityManager.createNativeQuery(nativeQuery);

            query.setParameter(1, employee.getId());
            query.setParameter(2, employee.getName());
            query.setParameter(3, employee.getCountry());
            query.setParameter(4, employee.getEmail());
            query.setParameter(5, employee.getAddresses());
            query.setParameter(6, employee.getGender());
            query.setParameter(7, employee.getIsDeleted());

            query.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return employee;
    }

    /**
     * @return
     */
    @Override
    @Transactional //jakarta
    public Set<String> findAllCountriesWithJpa() {
        return entityManager.createQuery("select distinct country from Employee", String.class).getResultStream().collect(Collectors.toSet());
    }

    /**
     * @param id
     * @param employee
     * @return
     */
    @Override
    @Transactional //jakarta
    public Employee updateByIdWithJpa(Integer id, Employee employee) {
        Employee refreshEmployee = Optional.ofNullable(entityManager.find(Employee.class, id))
                .orElseThrow(() -> new RuntimeException("id = " + employee.getId()));
        return entityManager.merge(refreshEmployee);
    }

    /**
     * @param id
     */
    @Override
    @Transactional //jakarta
    public String deleteByIdWithJpa(Integer id) {
        Optional<Employee> employee = Optional.ofNullable(entityManager.find(Employee.class, id));
        entityManager.remove(employee);
        return "remove Employee with jpa by id";
    }

    @Override
    @Transactional
    public List<Employee> getAllEM() {
        return entityManager.createNativeQuery("SELECT * FROM users", Employee.class).getResultList();
    }
}

package com.example.demowithtests;

import com.example.demowithtests.domain.Document;
import com.example.demowithtests.domain.Employee;
import com.example.demowithtests.domain.Gender;
import com.example.demowithtests.repository.CrudEmployeeRepository;
import com.example.demowithtests.repository.EmployeeRepository;
import com.example.demowithtests.service.EmployeeServiceBean;
import com.example.demowithtests.util.exception.ResourceNotFoundException;
import jakarta.persistence.EntityNotFoundException;
import org.h2.mvstore.Page;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@DisplayName("Employee Service Tests")
public class ServiceTests {

    @Mock
    private EmployeeRepository employeeRepository;
    @Mock
    private CrudEmployeeRepository crudEmployeeRepository;

    @InjectMocks
    private EmployeeServiceBean service;

    private Employee employee;


    @BeforeEach
    void setUp() {
        employee = Employee
                .builder()
                .id(1)
                .name("Mark")
                .country("UK")
                .email("test@mail.com")
                .gender(Gender.M)
                .isDeleted(false)
                .build();
    }

    @Test
    @DisplayName("Save employee test")
    public void whenSaveEmployee_shouldReturnEmployee() {

        when(crudEmployeeRepository.save(ArgumentMatchers.any(Employee.class))).thenReturn(employee);
        var created = service.create(employee);
        assertThat(created.getName()).isSameAs(employee.getName());
        verify(crudEmployeeRepository).save(employee);
    }

    @Test
    @DisplayName("Get employee by exist id test")
    public void whenGivenId_shouldReturnEmployee_ifFound() {

        Employee employee = new Employee();
        employee.setId(88);
        when(employeeRepository.findById(employee.getId())).thenReturn(Optional.of(employee));
        Employee expected = service.getById(employee.getId());
        assertThat(expected).isSameAs(employee);
        verify(employeeRepository).findById(employee.getId());
    }

    @Test
    @DisplayName("Throw exception when employee not found test")
    public void should_throw_exception_when_employee_doesnt_exist() {
        when(employeeRepository.findById(anyInt())).thenThrow(ResourceNotFoundException.class);
        assertThrows(ResourceNotFoundException.class, () -> employeeRepository.findById(anyInt()));
    }

    @Test
    @DisplayName("Read employee by id test")
    public void readEmployeeByIdTest() {

        when(employeeRepository.findById(employee.getId())).thenReturn(Optional.of(employee));
        Employee expected = service.getById(employee.getId());
        assertThat(expected).isSameAs(employee);
        verify(employeeRepository).findById(employee.getId());
    }

    @Test
    @DisplayName("Read all employees test")
    public void readAllEmployeesTest() {

        when(employeeRepository.findAll()).thenReturn(List.of(employee));
        var list = employeeRepository.findAll();
        assertThat(list.size()).isGreaterThan(0);
        verify(employeeRepository).findAll();
    }

    @Test
    @DisplayName("Delete employee test")
    public void deleteEmployeeTest() {
        when(employeeRepository.findById(employee.getId())).thenReturn(Optional.of(employee));
        service.removeById(employee.getId());
        verify(employeeRepository).save(employee);
        assertTrue(employee.getIsDeleted());
    }

    @Test
    @DisplayName("Test - count By Email Length Less Than Equal Test")
    public void countByEmailLengthLessThanEqualTest(){

        Employee employee = new Employee();
        employee.setAddresses(new HashSet<>());
        employee.setCountry("GB");
        employee.setDocument(new Document());
        employee.setEmail("jane.doe@example.org");
        employee.setGender(Gender.M);
        employee.setId(1);
        employee.setIsDeleted(true);
        employee.setName("Name");

        int maxLength = 15;
        when(employeeRepository.countByEmailLengthLessThanEqual(anyInt())).thenReturn(3);
        Integer count = service.countByEmailLengthLessThanEqual(maxLength);
        assertNotNull(count);
        assertEquals(3, count.intValue());

        verify(employeeRepository, times(1))
                .countByEmailLengthLessThanEqual(maxLength);
    }

    @Test
    public void updateEmployeeByName(){
        doNothing().when(crudEmployeeRepository).updateEmployeeByName(Mockito.<String>any(), Mockito.<Integer>any());
        service.updateEmployeeByName("Name", 1);
        verify(crudEmployeeRepository).updateEmployeeByName(Mockito.<String>any(), Mockito.<Integer>any());
    }

    @Test
    public void testGetAllEmployeeCountry() {
        when(employeeRepository.findAll()).thenReturn(new ArrayList<>());
        List<String> actualAllEmployeeCountry = service.getAllEmployeeCountry();
        verify(employeeRepository).findAll();
        assertTrue(actualAllEmployeeCountry.isEmpty());
    }

    @Test
    public void testGetAllEmployeeCountryError() {
        when(employeeRepository.findAll()).thenThrow(new EntityNotFoundException("An error occurred"));
        assertThrows(EntityNotFoundException.class, () -> service.getAllEmployeeCountry());
        verify(employeeRepository).findAll();
    }

    @Test
    void testFindByNameLengthLessThanEqual() {
        Optional<List<Employee>> result = Optional.of(new ArrayList<>());
        when(employeeRepository.findByNameLengthLessThanEqual(anyInt())).thenReturn(result);
        Optional<List<Employee>> actual = employeeRepository.findByNameLengthLessThanEqual(10);
        verify(employeeRepository).findByNameLengthLessThanEqual(anyInt());
        assertTrue(actual.isPresent());
        assertSame(result, actual);
    }

    @Test
    public void findByNameContainingTest(){
        when(employeeRepository.findByNameLengthLessThanEqual(anyInt()))
                .thenThrow(new EntityNotFoundException("An error occurred"));
        assertThrows(EntityNotFoundException.class, () ->
                service.findByNameLengthLessThanEqual(10));
        verify(employeeRepository).findByNameLengthLessThanEqual(anyInt());
    }

    @Test
    public void updateById(){
        var employeeNew = Employee
                .builder()
                .id(1)
                .name("Mark")
                .country("UK")
                .email("test@mail.com")
                .gender(Gender.M)
                .isDeleted(false)
                .build();
        assertThrows(EntityNotFoundException.class, () -> service.updateById(1, employeeNew));
    }


}

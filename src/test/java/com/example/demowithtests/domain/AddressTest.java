package com.example.demowithtests.domain;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AddressTest {
    @Test
    @DisplayName("Test All Arguments Constructors")
    void testAllArgsConstructor(){
        Address address = new Address(1L, true,
                "Country", "City", "Street");
        assertNotNull(address);
        assertEquals(1L, address.getId());
        assertTrue(address.getAddressHasActive());
        assertEquals("Country", address.getCountry());
        assertEquals("City", address.getCity());
        assertEquals("Street", address.getStreet());
    }

    @Test
    @DisplayName("Test toString Method")
    void testToString() {
        Address address = new Address(1L, true, "Country",
                "City", "Street");
        String expectedToString = "Address(id=1, addressHasActive=true, " +
                "country=Country, city=City, street=Street)";
        assertEquals(expectedToString, address.toString());
    }
}